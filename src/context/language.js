import React from 'react';

export const languages = {
  PL: 'pl',
  EN: 'en',
};

export const LanguageContext = React.createContext({
  language: languages.PL,
  setLanguage: () => {},
});