import 'whatwg-fetch';

export function doRequest(...args) {
  return fetch(...args);
}