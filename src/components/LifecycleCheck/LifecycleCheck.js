import React from 'react';

class LifecycleCheck extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      mount: true,
    }
  }

  render() {
    return (
      <div>
        {this.state.mount && (
          <Cmp name="parent">
            <Cmp name="child">
              <Cmp name="child.child"/>
            </Cmp>
          </Cmp>
        )}
        <button onClick={() => this.setState({mount: false})}>Unmount</button>
      </div>
    )
  }
}

export default LifecycleCheck;

class Cmp extends React.Component {
  constructor(props) {
    super(props);

    console.log('Constructor', props.name);
  }

  render() {
    console.log('render', this.props.name);

    return (
      <div>
        {this.props.name}
        {this.props.children}
      </div>
    );
  }

  componentDidMount() {
    console.log('didMount', this.props.name);
  }

  componentWillUnmount() {
    console.log('willUnmount', this.props.name);
  }
}