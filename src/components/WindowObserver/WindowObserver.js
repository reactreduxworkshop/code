import React from 'react';

class WindowObserver extends React.Component {
  constructor(props) {
    super(props);

    this.state = this._getWindowSize();
  }

  render() {
    return this.props.children(this.state.width, this.state.height);
  }

  componentDidMount() {
    window.addEventListener('resize', this._handleResize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this._handleResize);
  }

  _handleResize = () => {
    this.setState(this._getWindowSize());
  };

  _getWindowSize = () => {
    return {
      width: window.innerWidth,
      height: window.innerHeight
    }
  }
}

export default WindowObserver;