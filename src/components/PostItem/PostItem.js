import React from 'react';
import PropTypes from 'prop-types';

function PostItem(props) {
  console.log('PostItem render');
  return (
    <div>
      {props.post.id}: {props.post.title}
    </div>
  )
}

PostItem.propTypes = {
  post: PropTypes.shape({
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
  })
};

// PropTypes are ignored (BUG): https://github.com/facebook/react/issues/14159

const PostItemMemoized = React.memo(PostItem);

export default PostItemMemoized;