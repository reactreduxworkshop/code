import React from 'react';

class HeaderErrorBoundary extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      failed: false,
    }
  }

  static getDerivedStateFromError(error) {
    return {failed: true};
  }

  render() {
    if (this.state.failed) {
      return <span>Sorry. We can't render content</span>
    }

    return this.props.children;
  }

  componentDidCatch(error, info) {
    // we have to log error
    console.log('children failed', error, info)
  }
}

export default HeaderErrorBoundary;