import React from 'react';
import ReactDOM from 'react-dom';

import {LanguageContext, languages} from "../../context/language";

const TRANSLATION = {
  [languages.PL]: 'Pokaż modal',
  [languages.EN]: 'Show modal',
};

class Modal extends React.Component {
  static contextType = LanguageContext;

  constructor(props) {
    super(props);

    this.state = {
      showModal: false,
    };

    this.modalsBoxNode = null;
    this.domNode = document.createElement('div');
  }

  render() {
    return (
      <div style={{position: 'relative', height: 100, width: 100, background: 'red'}}>

        <button onClick={() => this.setState({showModal: true})}>{TRANSLATION[this.context.language]}</button>

        {this.state.showModal && (
          ReactDOM.createPortal(
            <div style={{
              position: 'absolute',
              background: 'rgba(0, 0, 255, 0.5)',
              width: '100%',
              height: '100%',
              top: 0,
              left: 0,
              zIndex: 1
            }}
            onClick={() => this.setState({showModal: false})}>
              Modal content
            </div>,
            this.domNode
          )
        )}
      </div>
    )
  }

  componentDidMount() {
    this.modalsBoxNode = document.getElementById('modals-box');
    this.modalsBoxNode.appendChild(this.domNode);
  }

  componentWillUnmount() {
    this.modalsBoxNode.removeChild(this.domNode);
  }
}

export default Modal;