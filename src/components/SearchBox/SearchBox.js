import React from 'react';

import Button from "../Button";

class SearchBox extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      searchVal: '',
    };

    this.inputRef = React.createRef();
  }

  render() {
    return (
      <div>
        <input ref={this.inputRef} value={this.state.searchVal} onChange={this._handleChange}/>
        <Button onClick={() => alert(this.state.searchVal)}>Search</Button>
        <Button onClick={this._handleClear}>Clear</Button>
      </div>
    )
  }

  componentDidMount() {
    this.inputRef.current.focus();
  }

  _handleChange = event => {
    this.setState({
      searchVal: event.target.value,
    })
  };

  _handleClear = () => {
    this.setState({
      searchVal: '',
    })
  }
}

export default SearchBox;