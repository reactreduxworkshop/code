import React from 'react';
import PropTypes from 'prop-types';

import Button from "../Button";

function Counter(props) {
  return (
    <div>
      <div>Counter: {props.counter}</div>
      <Button onClick={props.onClick}>Inc</Button>
    </div>
  )
}

Counter.propTypes = {
  onClick: PropTypes.func,
  counter: PropTypes.number
};

Counter.displayName = 'Counter';

export default Counter;