import React from 'react';
import PropTypes from 'prop-types';

function Button(props) {
  return (
    <button className={props.className} onClick={props.onClick}>{props.children}</button>
  )
}

Button.displayName = 'Button';
Button.defaultProps = {
  children: 'Click me!',
};
Button.propTypes = {
  children: PropTypes.string,
  className: PropTypes.string,
  onClick: PropTypes.func.isRequired,
};

export default Button;