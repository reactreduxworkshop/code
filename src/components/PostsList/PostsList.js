import React from 'react';
// import PropTypes from 'prop-types';

import {doRequest} from "../../services/fetch";
import PostItem from "../PostItem";

class PostsList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      posts: null,
      loading: false,
    }
  }

  render() {
    return (
      <div className="posts-list">
        {this.state.loading ? 'loading...' : this._getPostItems(this.state.posts)}
      </div>
    )
  }

  componentDidMount() {
    this._fetchPosts();
  }

  _fetchPosts = () => {
    this._setLoading(true);

    doRequest('https://jsonplaceholder.typicode.com/todos')
      .then(response => response.json())
      .then(this._attachPosts)
      .then(() => this._setLoading(false));
  };

  _attachPosts = posts => {
    this.setState({
      posts,
    });
  };

  _setLoading = loading => {
    this.setState({
      loading,
    });
  };

  _getPostItems = posts => {
    return posts && posts.map(post => <PostItem key={post.id} post={post}/>)
  }
}

PostsList.propTypes = {};
PostsList.displayName = 'PostsList';

export default PostsList;