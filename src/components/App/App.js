import React, {Suspense} from 'react';

import './App.css';
import logo from './logo.svg';
import Counter from "../Counter";
import Header from "../Header/Header";
import SearchBox from "../SearchBox";
import PostsList from "../PostsList";
import HeaderErrorBoundary from "../HeaderErrorBoundary";
import Modal from "../Modal";

import {LanguageContext, languages} from '../../context/language';
import Tabs from "../Tabs/Tabs";
import Tab from "../Tab/Tab";


const LifecycleCheck = React.lazy(() => import("../LifecycleCheck"));
const WindowObserver = React.lazy(() => import("../WindowObserver"));

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      counter: 0,
      languageContext: {
        language: languages.PL,
        setLanguage: this._setLanguage,
      }

    }
  }

  render() {
    return (
      <div className="app">
        <div id="modals-box"/>

        <LanguageContext.Provider value={this.state.languageContext}>
          <Suspense fallback={<div>Loading...</div>}>
            <LifecycleCheck/>
          </Suspense>

          <Suspense fallback={<div>Loading...</div>}>
            <WindowObserver>
              {
                (width, height) => width > 700 ? 'Duży' : 'Mały'
              }
            </WindowObserver>
          </Suspense>

          <HeaderErrorBoundary>
            <Header>Nasz nagłówek</Header>
          </HeaderErrorBoundary>
          <div>
            <img src={logo} className="app__logo" alt="logo"/>
          </div>
          {this.state.counter > 5 ? 'done' : <Counter onClick={this._handleClick} counter={this.state.counter}/>}
          <br/>
          <Modal/>
          <br/>
          <Tabs>
            <Tab id="1" title="first">Content1</Tab>
            <Tab id="2" title="second">Content2</Tab>
            <Tab id="3" title="third">Content3</Tab>
            <button>Test</button>
          </Tabs>
          <br/>
          <SearchBox/>
          <PostsList/>

        </LanguageContext.Provider>
      </div>
    )
  }

  _handleClick = () => {
    this.setState(state => ({
      counter: state.counter + 1,
    }));
    this.setState(state => ({
      counter: state.counter + 1,
    }));
  };

  _setLanguage = language => {
    this.setState({
      languageContext: {
        language,
        setLanguage: this._setLanguage,
      }
    });
  }
}

export default App;