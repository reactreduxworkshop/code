import React from 'react';

class Tabs extends React.Component {
  static displayName = 'Tabs';

  constructor(props) {
    super(props);

    this.state = {
      selectedTabId: props.defaultSelected ? props.defaultSelected : React.Children.toArray(this.props.children)[0].props.id
    }
  }

  render() {
    const filteredChildrenArr = React.Children.toArray(this.props.children).filter(child => {
      return child.type.displayName === 'Tab';
    });

    const selectedTab = filteredChildrenArr.find(child => child.props.id === this.state.selectedTabId);

    return (
      <div>
        <div>
          {filteredChildrenArr.map(child => <span onClick={() => this._handleTabChange(child.props.id)} key={child.props.id} style={{marginRight: 10, cursor: 'pointer'}}>{child.props.title}</span>)}
        </div>
        <div>
          {React.cloneElement(
            selectedTab,
            {
              // it shallowly merges props like setState merges state
            },
            selectedTab.props.children + ' injected'
          )}
        </div>
      </div>
    )
  }

  _handleTabChange = id => {
    this.setState({
      selectedTabId: id,
    });
  }
}

export default Tabs;