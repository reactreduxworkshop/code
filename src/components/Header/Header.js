import React from 'react';
import {LanguageContext, languages} from "../../context/language";

class Header extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      throwNow: false,
    }
  }

  render() {
    if (this.state.throwNow) {
      throw new Error('header thrown');
    }

    return (
      <header>
        {this.props.children}
        <button onClick={() => this.setState({throwNow: true})}>Throw</button>
        <LanguageContext.Consumer>
          {
            ({language, setLanguage}) => {
              return Object.values(languages).map(language => {
                return (
                  <button key={language} onClick={() => setLanguage(language)}>{language}</button>
                )
              })
            }
          }
        </LanguageContext.Consumer>
      </header>
    )
  }

  componentDidUpdate() {
    console.log('Header updated');
  }
}

export default Header;