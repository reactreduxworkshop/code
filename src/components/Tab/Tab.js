import React from 'react';

function Tab(props) {
  return (
    <div>
      {props.children}
    </div>
  )
}

Tab.displayName = 'Tab';

export default Tab;